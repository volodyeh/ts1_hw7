import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SearchPage {

    void applyContentTypeAndDateToFilters(WebDriver driver) {
        List<WebElement> checkboxesSpans = driver.findElements(By.className("app-search-filter__filter-name"));
        for (WebElement checkboxesSpan : checkboxesSpans) {
            if (Objects.equals(checkboxesSpan.getText(), "Article")) {
                checkboxesSpan.click();
                break;
            }
        }

        driver.findElement(By.cssSelector("#date-from")).sendKeys("2024");
        driver.findElement(By.cssSelector("#date-to")).sendKeys("2024");
        driver.findElement(By.cssSelector("#popup-filters > div.app-filter-buttons > button.eds-c-button.eds-c-button--primary"))
                .click();
    }

    ArrayList<ArticleData> getFirstFourArticles(WebDriver driver) {
        ArrayList<ArticleData> articles = new ArrayList<>();

        List<WebElement> articlesTitles = driver
                .findElements(By.cssSelector("#main > div > div:nth-child(4) > div > div:nth-child(2) > div:nth-child(2) > ol > li > div > div > h3 > a > span"))
                .stream()
                .limit(4)
                .collect(Collectors.toList());
        List<WebElement> articlesLinks = driver
                .findElements(By.className("app-card-open__link"))
                .stream()
                .limit(4)
                .collect(Collectors.toList());
        List<WebElement> articlesDates = driver
                .findElements(By.className("c-meta__item"))
                .stream()
                .filter(webElement -> Objects.equals(webElement.getAttribute("data-test"), "published"))
                .limit(4)
                .collect(Collectors.toList());

        for (int i = 0; i < articlesLinks.size(); i++) {
            ArticleData articleData = new ArticleData(
                    articlesTitles.get(i).getText(),
                    articlesLinks.get(i).getAttribute("href"),
                    articlesDates.get(i).getText()
            );
            articles.add(articleData);
        }

        return articles;
    }

    void fillOutAndSubmitSearchField(WebDriver driver) {
        driver.findElement(By.cssSelector("#search-springerlink")).sendKeys("Page AND Object AND Model AND (Sellenium OR Testing)");
        driver.findElement(By.cssSelector("#search-submit")).click();
    }

}
