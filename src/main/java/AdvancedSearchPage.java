import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class AdvancedSearchPage {

    void fillOutAndSubmitAdvancedSearchForm(WebDriver driver) {
        driver.findElement(By.cssSelector("#all-words")).sendKeys("Page Object Model");
        driver.findElement(By.cssSelector("#least-words")).sendKeys("Sellenium Testing");
        new Select(driver.findElement(By.cssSelector("#date-facet-mode"))).selectByVisibleText("in");
        driver.findElement(By.cssSelector("#facet-start-year")).sendKeys("2024");
        driver.findElement(By.cssSelector("#submit-advanced-search")).click();
    }

}
