public class ArticleData {
    String title;
    String doi;
    String date;

    public ArticleData(String title, String doi, String date) {
        this.title = title;
        this.doi = doi;
        this.date = date;
    }
}
