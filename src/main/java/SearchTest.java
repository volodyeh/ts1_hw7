import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SearchTest {

    private WebDriver driver;
    LoginPage loginPage;
    AdvancedSearchPage advancedSearchPage;
    SearchPage searchPage;

    @AfterAll
    public void tearDown(){
        driver.quit();
    }

    public Stream<Arguments> provideArticles() throws InterruptedException {
        ArrayList<ArticleData> articles;
        ArrayList<ArticleData> articlesAfterLogin;

        loginPage = new LoginPage();
        advancedSearchPage = new AdvancedSearchPage();
        searchPage = new SearchPage();

        System.setProperty("webdriver.chrome.driver", "/Users/erwinelder/Downloads/chromedriver-mac-arm64/chromedriver");
        driver = new ChromeDriver();

        driver.get("https://link.springer.com/advanced-search");

        advancedSearchPage.fillOutAndSubmitAdvancedSearchForm(driver);

        Thread.sleep(1000);

        driver.findElement(By.cssSelector("body > dialog > div.cc-banner__content > div > div.cc-banner__footer > button.cc-button.cc-button--secondary.cc-button--contrast.cc-banner__button.cc-banner__button-reject"))
                .click();

        searchPage.applyContentTypeAndDateToFilters(driver);

        Thread.sleep(1000);

        articles = searchPage.getFirstFourArticles(driver);

        loginPage.login(driver);

        Thread.sleep(1000);

        searchPage.fillOutAndSubmitSearchField(driver);

        Thread.sleep(1000);

        searchPage.applyContentTypeAndDateToFilters(driver);

        Thread.sleep(2000);

        articlesAfterLogin = searchPage.getFirstFourArticles(driver);

        return IntStream.range(0, Math.min(articles.size(), articlesAfterLogin.size()))
                .mapToObj(i -> Arguments.of(articles.get(i), articlesAfterLogin.get(i)));
    }

    @ParameterizedTest
    @MethodSource("provideArticles")
    public void testSearchWithArguments(ArticleData expectedArticle, ArticleData actualArticle) {
        assertEquals(expectedArticle.title, actualArticle.title);
        assertEquals(expectedArticle.doi, actualArticle.doi);
        assertEquals(expectedArticle.date, actualArticle.date);
    }

}
